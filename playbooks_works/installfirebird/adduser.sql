connect security.db user sysdba;

-- ДОБАВЛЯЕТ ЛОГИРОВАНИЕ ПРИ ИСПОЛЬЗОВАНИИ ВНЕШНЕГО СКРИПТА ISQL
SET COUNT ON;
SET PLAN ON;
SET BAIL ON;
SET ECHO ON;

-- adding new user 
create user hack2 password 'hack2';
grant rdb$admin to hack2;
commit;

